function SinhVien(
    _maSV,
    _tenSV,
    _emailSV,
    _passSV,
    _diemToan,
    _diemLy,
    _diemHoa,  

){
    this.maSV=_maSV
    this.tenSV =_tenSV
    this.emailSV=_emailSV
    this.passSV=_passSV
    this.diemToan=_diemToan
    this.diemLy=_diemLy
    this.diemHoa=_diemHoa
    this.tinhDTB=function(){
        return ((this.diemToan +this.diemHoa +this.diemLy)/3).toFixed(2)
    }
}