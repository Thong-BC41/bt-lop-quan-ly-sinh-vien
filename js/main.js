var DSSV = [];
//lấy dữ liệu từ LocalStorage khi user load
let dataJson = localStorage.getItem("DSSV_LOCAL");
// DSSV= dataJson ? JSON.parse(dataJson) : [   ]
//kiểm tra nếu có data ở localStorage thì mới gán vào array DSSV
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  DSSV = dataArr.map(function (item) {
    var sv = new SinhVien(
      item.maSV,
      item.tenSV,
      item.emailSV,
      item.passSV,
      item.diemToan,
      item.diemLy,
      item.diemHoa,
    );
    return sv;
  });
  renderDSSV(DSSV);
}

//thêm SV
function themSV() {
  var sv = layThongTinTuForm();
  // push sv vào DSSV
  DSSV.push(sv);

  //convert array DSSV thành json
  var dssvJson = JSON.stringify(DSSV);


  //lưu json vào localtorage
  localStorage.setItem("DSSV_LOCAL", dssvJson);

  renderDSSV(DSSV);
}

//xóa SV
function xoaSV(idSV) {
  // splice(viTri,1)
  var viTri = timKiemViTri(idSV, DSSV);
  if (viTri != -1) {
    DSSV.splice(viTri, 1);
    renderDSSV(DSSV);
  }
}

function suaSV(idSV) {
  var viTri = timKiemViTri(idSV, DSSV);
  if (viTri != -1) {
    var sv = DSSV[viTri];
    console.log(sv);
    document.getElementById("txtMaSV").disabled = true;
  }
}
